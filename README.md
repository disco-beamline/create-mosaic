# Create mosaïc

Different solutions to recreate mosaics of tiles of images from µManager, including correction of the illumination and camera field;

## TODO
- [ ] Create 2 subprojects
    - ImageJ jython script (tested on macos)
    - Matlab scripts (external executable provided for macos)
- [ ] 


## Install

## Necessary files
- Dark : series of dark images corresponding to your set of filters with their respective acquisition times, recorded in the dark without the beam.
- Bightfield: a Z stack of brightfields: lame matthieu without any filter
- Your stack of images to reconstruct
- For more info, look at the respective HowTo in the subfolders.
    
## Known possible bugs
- During the acquisition process in µManager, if you did not check "Save in separate files" your stack of images will be saved in a single file impossible to reconstruct. In that case, you may open it again with µManager and "Save as separate files"
- Missing Metadata.

## Reconstruct your mosaïc

## Authors
- Marie-Françoise Devaux
- Fred JammeS
- Hugo Chauvet
