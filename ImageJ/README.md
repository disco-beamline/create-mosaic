# Install 

- Download the folder [as zip](https://gitlab.com/discosoleil/create-mosaic/-/archive/master/create-mosaic-master.zip?path=ImageJ)
- Extract the content to a folder in your computer
- **Edit the file script_mosaic_telemos.py** and change the path at line **26**:
  ``` python
  basedir = os.path.expanduser("~/Documents/Boulo/ligne_DISCO/create-mosaic/ImageJ/")
  ```
  with your path to the folder containing the folder **utils**, (i.e. the path where you have unzip the package)
  
## Install as a FiJi Plugins

1. Locate the folder where your ImageJ is installed (For mac users it could be locate on the Applications forlder, right click on FiJi and then "Show Package Contents" to access the FiJi folder)
2. Add a folder **disco_ij** in the **Fiji.app/plugins** folder an copy the **script_mosaic_telemos.py** into the **disco_ij** folder.
3. Copy the **utils** folder into **Fiji.app/jars/Lib** directory
4. In Fiji **help>refresh menus** and **restart FiJi*

# Procedure
  - Open FIJI
  - Open the script **script_mosaic_telemos.py**
  - Run
  - Select the filter you want to reconstruct
  - Choose the respective folders for your data, your darks, your   DarkofWhite, your whites
  - Choose your options: 
    - Optimize: **not recommended** on TELEMOS, could create artifacts
    - BASIC plugin to correct image: works on some images, provide "pretty" results, **not quantitative!**

# Necessary files and folders
  - Your data: one folder of data recorded on µManager and Saved in separate files. Shall be a collection of subfolder for each tile of the mosaic.
  - Darks: One folder with a Collection of images corresponding to the filter set used with the right accumulation times for each respective filter and without the opening of the shutter or the ambiant light.
  - DarkofWhite: One file, Dark (no shutter, no ambiant light) with the conditions of the Whites (only one plane)
  - Whites: One folder of images corresponding to : No filter, stack of +/- 100 µm with 1 µm step of the LameMatthieu at 100 ms.
