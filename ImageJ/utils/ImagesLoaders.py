"""
Contain function or class to make import of image sequences easier in ImageJ
"""
import os
from path_tools import find_groups_values
from string import Formatter
from itertools import product

from loci.formats import ChannelSeparator
from ij import ImagePlus, VirtualStack, IJ


def load_image_sequence(rootdir, path_pattern, order=None, channel_group=None,
                        z_group=None, return_filenames=False):
    """
    THIS ONE IS BUGGY FOR MM2 format
    
    Load an image sequence (several tiff files in several sub-folders) as an ImageJ VirtualStack.
    It uses the python string format to define the pattern of file names
    If order is specified as a list of pattern keys, image will be loaded in that order,
    otherwise the order are the one in the pattern file names.

    Parameters:
    -----------

    rootdir: string,
        The fixed part of the path to image folder and subfolders
    path_pattern: string,
        The moving part of the folder/file_names.ext where you can use
        python string format convention.
    order: list of string or None,
        The order of pattern keys in which images should be loaded
    channel_group: string or None,
        The key which contains the color-band dimension of your stack
    z_group: string or None,
        The key of the z dimension of your stack
    return_filenames: boolean,
        If true return the list of file names

    Examples:
    ---------

    TODO
    """

    # Check that path_pattern doest not start with a /
    if path_pattern.startswith(os.path.sep):
        path_pattern = path_pattern[len(os.path.sep):]

    # Find group values
    gp_values = find_groups_values(os.path.join(rootdir, path_pattern))

    if order is None:
        keys = list(i[1] for i in Formatter().parse(path_pattern))
        keys_order = []
        for k in keys:
            if k is not None and k not in keys_order:
                keys_order += [k]

    else:
        keys_order = order

    # Set some smath things (if key is c or channel or filter set as channel_group, or if key is z set as z_group)
    if z_group is None:
        for t in ['z', 'Z']:
            if t in keys_order:
                z_group = t
                break

    if channel_group is None:
        for t in ['c', 'channel', 'channels', 'filter', 'filters']:
            if t in keys_order:
                channel_group = t
                break

            if t.upper() in keys_order:
                channel_group = t.upper()
                break

            if t.capitalize() in keys_order:
                channel_group = t.capitalize()
                break

    # Z should be before channel group but at the end (if given for hyperstack dimension)
    if z_group is not None:
        keys_order.remove(z_group)
        keys_order += [z_group]

    # If a channel group is given (need to be at the end of keys_order)
    if channel_group is not None:
        keys_order.remove(channel_group)
        keys_order += [channel_group]

    # Create the combination of all possible groups values
    allpattern_values = list(product(*(gp_values[k] for k in keys_order)))

    # Generate the list of file names to load
    allnames = tuple(path_pattern.format(**dict(zip(keys_order, p))) for p in allpattern_values)

    # We need to load one image to get it's size
    fr = ChannelSeparator()
    fr.setGroupFiles(False)
    fr.setId(os.path.join(rootdir, allnames[0]))
    metadata = fr.getGlobalMetadata()
    width, height = fr.getSizeX(), fr.getSizeY()
    # Create a virtualStack
    vstack = VirtualStack(width, height, None, rootdir)
    for imgname in allnames:
        vstack.addSlice(imgname)
        if not os.path.isfile(os.path.join(rootdir,imgname)):
            print('file %s does not exist' % imgname)

    # Create the ImagePlus to display the stack
    imp = ImagePlus("%s" % rootdir, vstack)

    # Set the dimension of hyperstack if keys for color channels
    # and z channels are given
    if channel_group is not None:
        nchannel = len(gp_values[channel_group])
    else:
        nchannel = 1

    if z_group is not None:
        nslice = len(gp_values[z_group])
    else:
        nslice = 1

    # Compute the time from the product of not used channel
    not_used_keys = []
    for k in keys_order:
        if k != channel_group and k != z_group:
            not_used_keys += [k]

    ntimes = 1
    for k in not_used_keys:
        ntimes *= len(gp_values[k])

    imp.setDimensions(nchannel, nslice, ntimes)
    imp.setOpenAsHyperStack(True)

    # Try to set the physical size from Metadata
    try:
        IJ.run(imp,
               "Properties...",
               "pixel_width={resox} pixel_height={resoy} pixel_unit='um'".format(resox=1/metadata['XResolution'],
                                                                                 resoy=1/metadata['YResolution']));
    except:
        pass

    # Set auto contrast
    IJ.run(imp, "Enhance Contrast", "saturated=0.35");

    if return_filenames:
        return imp, allnames
    else:
        return imp