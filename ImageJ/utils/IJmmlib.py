# -*- coding: utf-8 -*-
"""
Usefull jython functions for micro manager and imagej
"""
import os
from ij import ImagePlus, VirtualStack, IJ
from mmlib import generate_file_list, load_metadata, get_dimensions, get_resolution, get_micromanager_version, get_axisorder


def load_mm_images(rootdir, roi=1, tile=True, pos=False, 
                   return_filenames=False):
    """
    Use the metadata and mmlib's functions to load images 
    files into an ImageJ VirtualStack
    """
    
    if tile:
        metadata = load_metadata(os.path.join(rootdir, 'roi%i_tile1'%int(roi), 'metadata.txt'))
    else:
        metadata = load_metadata(rootdir)
        
    mmversion = int(get_micromanager_version(metadata)[0])
    dimensions = get_dimensions(metadata, roi)
    if mmversion == 1:
        stack_order = ['position', 'z', 'time', 'channels']
    else:
        stack_order = get_axisorder(metadata)
        IJ.log('MM axis order')
        IJ.log(','.join(stack_order))
        # The python loader need a reverse order compared to MM
        stack_order = stack_order[::-1]
        stack_order[stack_order.index('channel')] = 'channels'
        
        # stack_order = ['position', 'time', 'channels', 'z']

    # if not tile and pos:
        # stack_order = ['position', 'time', 'z', 'channels']
    image_files = generate_file_list(metadata, rootdir, roi, tile, pos, stack_order)
    
    # Create the virtual stack
    vstack = VirtualStack(dimensions['width'], dimensions['height'], None, rootdir)
    
    # Loop over slides
    for imgname in image_files:
        vstack.addSlice(imgname)
        
        if not os.path.isfile(os.path.join(rootdir,imgname)):
            print('file %s does not exist' % imgname)

    # Create the ImagePlus to display the stack
    imp = ImagePlus("%s" % rootdir, vstack)

    # dim starts at one in java not zero
    # Size of channels
    nchannel = dimensions['channels']
    # Size of z 
    nslice = dimensions['z']
    
    # The rest of dimensions goes to time
    nother = (dimensions['time']) * (dimensions['position'])

    imp.setDimensions(nchannel, nslice, nother)
    imp.setOpenAsHyperStack(True)
    
    reso = get_resolution(metadata)
    
    # Try to set the physical size from Metadata
    try:
        IJ.run(imp,
               "Properties...",
               "pixel_width={reso} pixel_height={reso} pixel_unit='um'".format(reso=reso))
    except:
        pass
        
    # Set auto contrast
    IJ.run(imp, "Enhance Contrast", "saturated=0.35")
    if return_filenames:
        return imp, image_files
    else:
        return imp